package org.annotations.processor;

import static com.google.testing.compile.CompilationSubject.assertThat;
import static com.google.testing.compile.Compiler.javac;
import static javax.tools.StandardLocation.CLASS_OUTPUT;

import java.nio.charset.Charset;

import org.junit.Test;

import com.google.common.truth.StringSubject;
import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;

public class BindingsProcessorTest {

	@Test
	public void checkFirstSessionBeanGeneratedBindings(){
		Compilation c = javac().withProcessors(new BindingsProcessor()).compile(JavaFileObjects.forResource("FirstSessionBean.java"));
		assertThat(c).succeeded();
		assertThat(c).generatedFile(CLASS_OUTPUT, "META-INF/ibm-ejb-jar-bnd.xml");
		StringSubject ss = assertThat(c).generatedFile(CLASS_OUTPUT, "META-INF/ibm-ejb-jar-bnd.xml").contentsAsString(Charset.forName("UTF-8"));
		ss.containsMatch("<session name=\"FirstSessionBean\">\\s+<message-destination-ref name=\"SomeQueue\" binding-name=\"Queues.Some\"/>\\s+<resource-ref name=\"ConnectionFactoryTx\" binding-name=\"Jms.CorporateConnectionFactoryTx\"/>\\s+<resource-ref name=\"jdbc/Simple\" binding-name=\"jdbc.Simple\"/>\\s+</session>");
	}
	
	@Test
	public void checkFirstMDBeanGeneratedBindings(){
		Compilation c = javac().withProcessors(new BindingsProcessor()).compile(JavaFileObjects.forResource("FirstMDBean.java"));
		assertThat(c).succeeded();
		assertThat(c).generatedFile(CLASS_OUTPUT, "META-INF/ibm-ejb-jar-bnd.xml");
		StringSubject ss = assertThat(c).generatedFile(CLASS_OUTPUT, "META-INF/ibm-ejb-jar-bnd.xml").contentsAsString(Charset.forName("UTF-8"));
		ss.containsMatch("<message-driven name=\"FirstMDBean\">\\s+<jca-adapter activation-spec-binding-name=\"hellSenderActivationSpec\" destination-binding-name=\"Jms.queue.SendToHell\"/>\\s+</message-driven>");
	}
	
	@Test
	public void checkNamedBeanBindings(){
		Compilation c = javac().withProcessors(new BindingsProcessor()).compile(JavaFileObjects.forResource("NamedSessionBean.java"));
		assertThat(c).succeeded();
		assertThat(c).generatedFile(CLASS_OUTPUT, "META-INF/ibm-ejb-jar-bnd.xml");
		StringSubject ss = assertThat(c).generatedFile(CLASS_OUTPUT, "META-INF/ibm-ejb-jar-bnd.xml").contentsAsString(Charset.forName("UTF-8"));
		ss.containsMatch("<session name=\"nazwanyBean\">\\s+<resource-ref name=\"SomeFactoryTx\" binding-name=\"SomeConnectionTx\"/>\\s+</session>");
	}

}
