package org.annotations.processor;

import static com.google.testing.compile.CompilationSubject.assertThat;
import static com.google.testing.compile.Compiler.javac;
import static javax.tools.StandardLocation.CLASS_OUTPUT;

import java.nio.charset.Charset;

import org.junit.Test;

import com.google.common.truth.StringSubject;
import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;

public class ExtensionsProcessorTest {

	@Test
	public void checkBeanWithTimeoutBeanGeneratedExtensions(){
		Compilation c = javac().withProcessors(new ExtensionsProcessor()).compile(JavaFileObjects.forResource("BeanWithTimeoutBean.java"));
		assertThat(c).succeeded();
		assertThat(c).generatedFile(CLASS_OUTPUT, "META-INF/ibm-ejb-jar-ext.xml");
		StringSubject ss = assertThat(c).generatedFile(CLASS_OUTPUT, "META-INF/ibm-ejb-jar-ext.xml").contentsAsString(Charset.forName("UTF-8"));
		ss.containsMatch("<session name=\"BeanWithTimeoutBean\">\\s+<global-transaction transaction-time-out=\"667\"/>\\s+</session>");
	}
	
	@Test
	public void checkNamedBeanWithTimeoutBeanGeneratedExtensions(){
		Compilation c = javac().withProcessors(new ExtensionsProcessor()).compile(JavaFileObjects.forResource("NamedBeanWithTimeoutBean.java"));
		assertThat(c).succeeded();
		assertThat(c).generatedFile(CLASS_OUTPUT, "META-INF/ibm-ejb-jar-ext.xml");
		StringSubject ss = assertThat(c).generatedFile(CLASS_OUTPUT, "META-INF/ibm-ejb-jar-ext.xml").contentsAsString(Charset.forName("UTF-8"));
		ss.containsMatch("<session name=\"nazwanyTimeoutBean\">\\s+<global-transaction transaction-time-out=\"1313\"/>\\s+</session>");
	}
	
}
