import javax.annotation.Resource;
import javax.annotation.Resource.AuthenticationType;
import javax.ejb.Stateless;

import org.annotations.MessageDestinationBinding;
import org.annotations.ResourceBinding;

@Stateless(name="nazwanyBean")
public class NamedSessionBean {
	
	@Resource(name = "SomeFactoryTx")
	@ResourceBinding(bindingName="SomeConnectionTx")
	private String qcf;

}
