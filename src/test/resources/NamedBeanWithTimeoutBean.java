package sl.testapp.sessionbeans;

import javax.ejb.Stateless;

import org.annotations.TransactionTimeoutBinding;

@Stateless(name = "nazwanyTimeoutBean")
@TransactionTimeoutBinding(1313)
public class NamedBeanWithTimeoutBean {

}
