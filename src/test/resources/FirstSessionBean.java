package sl.testapp.sessionbeans;

import javax.annotation.Resource;
import javax.annotation.Resource.AuthenticationType;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.ejb.Stateless;

import org.annotations.MessageDestinationBinding;
import org.annotations.ResourceBinding;

@Stateless
public class FirstSessionBean {
	
	@Resource(name = "ConnectionFactoryTx")
	@ResourceBinding(bindingName="Jms.CorporateConnectionFactoryTx")
	private String qcf;

	@Resource(name = "SomeQueue")
	@MessageDestinationBinding(bindingName = "Queues.Some")
	private String putQueue;

	@Resource(authenticationType = AuthenticationType.CONTAINER, name = "jdbc/Simple")
	@ResourceBinding(bindingName="jdbc.Simple")
	private String vibank;
}
