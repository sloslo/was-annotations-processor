package sl.testapp.mdbeans;

import javax.ejb.MessageDriven;

import org.annotations.ActivationBinding;

import javax.ejb.ActivationConfigProperty;

@MessageDriven(activationConfig = { @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
							@ActivationConfigProperty(propertyName = "destination", propertyValue = "Jms.queue.SendToHell") })
@ActivationBinding(name="hellSenderActivationSpec")
public class FirstMDBean {

}
