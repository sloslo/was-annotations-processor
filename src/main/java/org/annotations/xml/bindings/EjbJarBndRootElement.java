package org.annotations.xml.bindings;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ejb-jar-bnd")
@XmlAccessorType(XmlAccessType.FIELD)
public class EjbJarBndRootElement {

	@XmlAttribute(name = "version")
	private String version = "1.0";

	@XmlAttribute(name = "xmlns")
	private String xmlns = "http://websphere.ibm.com/xml/ns/javaee";

	@XmlAttribute(name = "xsi:schemaLocation")
	private String schemaLocation = "http://websphere.ibm.com/xml/ns/javaee http://websphere.ibm.com/xml/ns/javaee/ibm-ejb-jar-bnd_1_0.xsd";
	
	@XmlAttribute(name = "xmlns:xsi")
	private String xsi = "http://www.w3.org/2001/XMLSchema-instance";
	
	@XmlElement(name = "session")
	private List<SessionDescription> sessionBeans = new ArrayList<SessionDescription>();

	@XmlElement(name = "message-driven")
	private List<MessageDrivenDescription> messageDrivenBeans = new ArrayList<MessageDrivenDescription>();

	public List<SessionDescription> getSessionBeans() {
		return sessionBeans;
	}

	public void setSessionBeans(List<SessionDescription> sessionBeans) {
		this.sessionBeans = sessionBeans;
	}

	public List<MessageDrivenDescription> getMessageDrivenBeans() {
		return messageDrivenBeans;
	}

	public void setMessageDrivenBeans(List<MessageDrivenDescription> messageDrivenBeans) {
		this.messageDrivenBeans = messageDrivenBeans;
	}

}
