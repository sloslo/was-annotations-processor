package org.annotations.xml.bindings;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class JcaAdapterDescription {
	
	@XmlAttribute(name="activation-spec-binding-name")
	private String activationSpecBindingName;
	
	@XmlAttribute(name="destination-binding-name")
	private String destinationBindingName;

	public String getActivationSpecBindingName() {
		return activationSpecBindingName;
	}

	public void setActivationSpecBindingName(String activationSpecBindingName) {
		this.activationSpecBindingName = activationSpecBindingName;
	}

	public String getDestinationBindingName() {
		return destinationBindingName;
	}

	public void setDestinationBindingName(String destinationBindingName) {
		this.destinationBindingName = destinationBindingName;
	}
	
}
