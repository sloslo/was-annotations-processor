package org.annotations.xml.bindings;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "message-driven")
public class MessageDrivenDescription {

	@XmlAttribute(required=true)
	private String name;

	@XmlElement(name = "jca-adapter")
	private JcaAdapterDescription jcaAdapter;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public JcaAdapterDescription getJcaAdapter() {
		return jcaAdapter;
	}

	public void setJcaAdapter(JcaAdapterDescription jcaAdapter) {
		this.jcaAdapter = jcaAdapter;
	}

}
