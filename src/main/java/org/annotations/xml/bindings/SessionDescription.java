package org.annotations.xml.bindings;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="session")
public class SessionDescription {
	
	@XmlAttribute(required=true)
	private String name;
	
	@XmlElement(name = "message-destination-ref")
	private List<MessageDestinationDescription> mdds = new ArrayList<MessageDestinationDescription>();
	
	@XmlElement(name="resource-ref")
	private List<ResourceDescription> rds = new ArrayList<ResourceDescription>();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MessageDestinationDescription> getMdds() {
		return mdds;
	}

	public void setMdds(List<MessageDestinationDescription> mdds) {
		this.mdds = mdds;
	}

	public List<ResourceDescription> getRds() {
		return rds;
	}

	public void setRds(List<ResourceDescription> rds) {
		this.rds = rds;
	}
	
	
	
	

}
