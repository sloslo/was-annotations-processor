package org.annotations.xml.bindings;


import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ListWrapper<T> {
	
	private List<T> lista = new ArrayList<T>();

	public ListWrapper(final List<T> newList) {
		lista = newList;
	}

	@XmlAnyElement(lax = true)
	public List<T> getItems() {
		return lista;
	}
	
	

}
