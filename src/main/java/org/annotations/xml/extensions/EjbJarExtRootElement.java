package org.annotations.xml.extensions;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "ejb-jar-ext")
@XmlAccessorType(XmlAccessType.FIELD)
public class EjbJarExtRootElement {
	@XmlAttribute(name = "version")
	private String version = "1.0";

	@XmlAttribute(name = "xmlns")
	private String xmlns = "http://websphere.ibm.com/xml/ns/javaee";

	@XmlAttribute(name= "xsi:schemaLocation")
	private String schemaLocation = "http://websphere.ibm.com/xml/ns/javaee http://websphere.ibm.com/xml/ns/javaee/ibm-ejb-jar-ext_1_0.xsd";
	
	@XmlAttribute(name = "xmlns:xsi")
	private String xsi = "http://www.w3.org/2001/XMLSchema-instance";
	
	@XmlElement(name = "session")
	private List<SessionDescription> sessionBeans = new ArrayList<SessionDescription>();

	public List<SessionDescription> getSessionBeans() {
		return sessionBeans;
	}

	public void setSessionBeans(List<SessionDescription> sessionBeans) {
		this.sessionBeans = sessionBeans;
	}

}


