package org.annotations.xml.extensions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "session")
public class SessionDescription {

	@XmlAttribute(required = true)
	private String name;

	@XmlElement(name = "global-transaction")
	private GlobalTransaction transaction;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GlobalTransaction getTransaction() {
		return transaction;
	}

	public void setTransaction(GlobalTransaction transaction) {
		this.transaction = transaction;
	}

}
