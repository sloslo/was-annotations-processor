package org.annotations.processor;

import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.Stateless;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import javax.tools.Diagnostic.Kind;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.annotations.ActivationBinding;
import org.annotations.MessageDestinationBinding;
import org.annotations.ResourceBinding;
import org.annotations.xml.bindings.EjbJarBndRootElement;
import org.annotations.xml.bindings.JcaAdapterDescription;
import org.annotations.xml.bindings.MessageDestinationDescription;
import org.annotations.xml.bindings.MessageDrivenDescription;
import org.annotations.xml.bindings.ResourceDescription;
import org.annotations.xml.bindings.SessionDescription;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.Iterators;

@SupportedAnnotationTypes({ "org.annotations.ResourceBinding", "org.annotations.ActivationBinding",
		"org.annotations.MessageDestinationBinding" })
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class BindingsProcessor extends AbstractProcessor {

	private List<MessageDrivenDescription> messageDrivenDefinitions;
	private List<SessionDescription> sessionDefinitions;

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		try {

			processingEnv.getMessager().printMessage(Kind.NOTE, "Processing for bindings");
			processingEnv.getMessager().printMessage(Kind.NOTE, Boolean.toString(roundEnv.processingOver()));

			if (!roundEnv.processingOver()) {
				sessionDefinitions = processSession(roundEnv);
				messageDrivenDefinitions = processActivations(roundEnv);
			} else {
				if (!messageDrivenDefinitions.isEmpty() || !sessionDefinitions.isEmpty()) {

					FileObject fo = processingEnv.getFiler().createResource(StandardLocation.CLASS_OUTPUT, "",
							"META-INF/ibm-ejb-jar-bnd.xml", (Element[]) null);
					OutputStream os = fo.openOutputStream();

					JAXBContext jc = JAXBContext.newInstance(EjbJarBndRootElement.class);
					EjbJarBndRootElement ejbre = new EjbJarBndRootElement();
					ejbre.setMessageDrivenBeans(messageDrivenDefinitions);
					ejbre.setSessionBeans(sessionDefinitions);

					Marshaller m = jc.createMarshaller();
					m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

					m.marshal(ejbre, os);

					os.flush();
					os.close();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			processingEnv.getMessager().printMessage(Kind.ERROR, "Something went wrong: " + e.getMessage());
		}

		return true;
	}

	private List<MessageDrivenDescription> processActivations(RoundEnvironment roundEnv) {
		List<MessageDrivenDescription> messageDrivenDefinitions = new ArrayList<MessageDrivenDescription>();
		processingEnv.getMessager().printMessage(Kind.NOTE,
				"ActivationBinding size = [" + roundEnv.getElementsAnnotatedWith(ActivationBinding.class).size() + "]");
		for (Element elem : roundEnv.getElementsAnnotatedWith(ActivationBinding.class)) {
			ActivationBinding simple = elem.getAnnotation(ActivationBinding.class);
			MessageDriven messageDriven3 = elem.getAnnotation(MessageDriven.class);

			final Optional<ActivationConfigProperty> oacp = Iterators.tryFind(
					Iterators.forArray(messageDriven3.activationConfig()), new Predicate<ActivationConfigProperty>() {
						@Override
						public boolean apply(final ActivationConfigProperty searchedElement) {
							return "destination".equals(searchedElement.propertyName());
						}
					});

			MessageDrivenDescription mdd = new MessageDrivenDescription();
			mdd.setName(elem.getSimpleName().toString());

			JcaAdapterDescription jad = new JcaAdapterDescription();
			jad.setActivationSpecBindingName(simple.name());
			if (oacp.isPresent()) {
				jad.setDestinationBindingName(oacp.get().propertyValue());
			}
			mdd.setJcaAdapter(jad);

			String message = "annotation found in " + elem.getSimpleName();
			processingEnv.getMessager().printMessage(Kind.NOTE, message);
			messageDrivenDefinitions.add(mdd);
		}
		return messageDrivenDefinitions;
	}

	private List<SessionDescription> processSession(RoundEnvironment roundEnv) {
		Map<String, MyTuple> descriptionsByBean = new HashMap<String, MyTuple>();
		List<SessionDescription> sessionDefinitions = new ArrayList<SessionDescription>();
		processingEnv.getMessager().printMessage(Kind.NOTE,
				"ResourceBinding size = [" + roundEnv.getElementsAnnotatedWith(ResourceBinding.class).size() + "]");
		processingEnv.getMessager().printMessage(Kind.NOTE, "MessageDestinationBinding size = ["
				+ roundEnv.getElementsAnnotatedWith(MessageDestinationBinding.class).size() + "]");
		for (Element elem : roundEnv.getElementsAnnotatedWith(ResourceBinding.class)) {
			TypeMirror classNameType = elem.getEnclosingElement().asType();
			Stateless statAnno = elem.getEnclosingElement().getAnnotation(Stateless.class);
			String classNameKey = classNameType.toString();
			if (!Strings.isNullOrEmpty(statAnno.name())) {
				classNameKey = statAnno.name();
			}

			if (!descriptionsByBean.containsKey(classNameKey)) {
				descriptionsByBean.put(classNameKey, new MyTuple());
			}
			MyTuple tuple = descriptionsByBean.get(classNameKey);
			ResourceBinding rb = elem.getAnnotation(ResourceBinding.class);
			Resource r = elem.getAnnotation(Resource.class);
			ResourceDescription rd = new ResourceDescription();
			rd.setBindingName(rb.bindingName());
			rd.setName(r.name());
			tuple.getResourceDescriptions().add(rd);
		}
		for (Element elem : roundEnv.getElementsAnnotatedWith(MessageDestinationBinding.class)) {
			TypeMirror classNameType = elem.getEnclosingElement().asType();
			Stateless statAnno = elem.getEnclosingElement().getAnnotation(Stateless.class);
			String classNameKey = classNameType.toString();
			if (!Strings.isNullOrEmpty(statAnno.name())) {
				classNameKey = statAnno.name();
			}

			if (!descriptionsByBean.containsKey(classNameKey)) {
				descriptionsByBean.put(classNameKey, new MyTuple());
			}
			MyTuple tuple = descriptionsByBean.get(classNameKey);
			MessageDestinationBinding rb = elem.getAnnotation(MessageDestinationBinding.class);
			Resource r = elem.getAnnotation(Resource.class);
			MessageDestinationDescription mdd = new MessageDestinationDescription();
			mdd.setBindingName(rb.bindingName());
			mdd.setName(r.name());
			tuple.getMessageDestinationDescriptions().add(mdd);
		}

		for (String key : descriptionsByBean.keySet()) {
			MyTuple mt = descriptionsByBean.get(key);
			SessionDescription sd = new SessionDescription();
			sd.setName(key.substring(key.lastIndexOf('.') + 1));
			sd.setRds(mt.getResourceDescriptions());
			sd.setMdds(mt.getMessageDestinationDescriptions());
			sessionDefinitions.add(sd);
		}
		return sessionDefinitions;
	}

}

class MyTuple {
	private List<MessageDestinationDescription> messageDestinationDescriptions = new ArrayList<MessageDestinationDescription>();
	private List<ResourceDescription> resourceDescriptions = new ArrayList<ResourceDescription>();

	public List<MessageDestinationDescription> getMessageDestinationDescriptions() {
		return messageDestinationDescriptions;
	}

	public void setMessageDestinationDescriptions(List<MessageDestinationDescription> messageDestinationDescriptions) {
		this.messageDestinationDescriptions = messageDestinationDescriptions;
	}

	public List<ResourceDescription> getResourceDescriptions() {
		return resourceDescriptions;
	}

	public void setResourceDescriptions(List<ResourceDescription> resourceDescriptions) {
		this.resourceDescriptions = resourceDescriptions;
	}

}
