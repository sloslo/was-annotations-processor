package org.annotations.processor;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.ejb.Stateless;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.annotations.TransactionTimeoutBinding;
import org.annotations.xml.extensions.EjbJarExtRootElement;
import org.annotations.xml.extensions.GlobalTransaction;
import org.annotations.xml.extensions.SessionDescription;

import com.google.common.base.Strings;

@SupportedAnnotationTypes({ "org.annotations.TransactionTimeoutBinding" })
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class ExtensionsProcessor extends AbstractProcessor {
	private java.util.List<SessionDescription> dataForOutput = new ArrayList<SessionDescription>();

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		try {
			processingEnv.getMessager().printMessage(Kind.NOTE, "Processing for extensions!");
			processingEnv.getMessager().printMessage(Kind.NOTE, Boolean.toString(roundEnv.processingOver()));

			if (!roundEnv.processingOver()) {
				processingEnv.getMessager().printMessage(Kind.NOTE, "TransactionTimeoutBinding size = ["
						+ roundEnv.getElementsAnnotatedWith(TransactionTimeoutBinding.class).size() + "]");
				for (Element elem : roundEnv.getElementsAnnotatedWith(TransactionTimeoutBinding.class)) {
					TransactionTimeoutBinding simple = elem.getAnnotation(TransactionTimeoutBinding.class);
					Stateless statAnno = elem.getAnnotation(Stateless.class);
					String classNameKey = elem.getSimpleName().toString();
					if(!Strings.isNullOrEmpty(statAnno.name())) {
						classNameKey = statAnno.name();
					}

					String message = "annotation found in " + classNameKey + " with simple";
					processingEnv.getMessager().printMessage(Kind.NOTE, message);

					SessionDescription sd = new SessionDescription();

					GlobalTransaction gt = new GlobalTransaction();
					gt.setTimeout(simple.value());
					sd.setName(classNameKey);
					sd.setTransaction(gt);

					dataForOutput.add(sd);
				}
			} else {

				if (!dataForOutput.isEmpty()) {

					FileObject fo = processingEnv.getFiler().createResource(StandardLocation.CLASS_OUTPUT, "",
							"META-INF/ibm-ejb-jar-ext.xml", (Element[]) null);
					OutputStream os = fo.openOutputStream();

					JAXBContext jc = JAXBContext.newInstance(EjbJarExtRootElement.class);
					EjbJarExtRootElement ejbre = new EjbJarExtRootElement();
					ejbre.setSessionBeans(dataForOutput);

					Marshaller m = jc.createMarshaller();
					m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

					m.marshal(ejbre, os);

					os.flush();
					os.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			processingEnv.getMessager().printMessage(Kind.ERROR, "Something went wrong: " + e.getMessage());
		}

		return true;
	}

}
